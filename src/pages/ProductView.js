import { useState, useEffect, useContext } from 'react';
import {Col, Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useParams } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	const {id} = useParams();
	let prod = {id}.id;
	console.log({id})
	
	useEffect(()=>{
		fetch(`https://frozen-bayou-69638.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData =>{	
			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])
	
	const purchase = () => {
		return (
			Swal.fire({
				icon: "success",
				title: 'Added to Cart!'
			})

		);
	};
	const redirect = () => {
		return (
			window.location.href = "/login"
			)
	}
	const redirect2 = () => {
		return (
			window.location.href = "/"
			)
	}
	console.log(user.isAdmin)
	return(
		<div className="min-vh-100 row pView">
			<div className="mt-5 col-md-12 col-lg-6 cardRow">
			
				<Col >
						<Card className="text-center cardTest">
							<Card.Body>
								 <Card.Img variant="top" className="viewProd" src={`/${prod}.JPG`}/>
							</Card.Body>
						</Card>			
				</Col>			
			</div>
			<div  className="mt-5 col-md-12  col-lg-5 cardRow">
			
				<Col>
				<h1> {productInfo.name}</h1>
				<hr/>
				<h5>
				{productInfo.description}</h5>
				<h3 className="my-4"> Price: ₱{productInfo.price} </h3>
					<Button variant="secondary" className="btn-block purch" 
							onClick={!user.id ? redirect :
								!user.isAdmin ? purchase : redirect2} >
							 Add to Cart
							</Button>
				</Col>			
			
			</div>
		</div>
		);
};