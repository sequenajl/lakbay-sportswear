import React, { Component } from 'react';
import {Carousel} from 'react-bootstrap';

import '../App.css';
//Pass down 'Props'


const textArray = ['Hiking', 'Swimming', 'Running', 'Biking'];


class Home extends Component {
    
  constructor() {
    super();
    this.state = { textIdx: 0 };
  }

  componentDidMount() {
    this.timeout = setInterval(() => {
      let currentIdx = this.state.textIdx;
      this.setState({ textIdx: currentIdx + 1 });
    }, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
  }

  render() {
    let textThatChanges = textArray[this.state.textIdx % textArray.length];

    return (
      <div>
    	<div className="testing min-vh-100">
	      <section id="flag">
	      
         
		        <h1 className="text-center mt-3 lakbayS">Lakbay Sportswear</h1>
            <h1 className="text-center lakbayS">Your Best Companion for </h1>
            
           
          
		        <h1 className="Hiker text-center "><span>{textThatChanges}</span></h1>
          
	      </section>
                <Carousel fade>
          <Carousel.Item className="carousels">
              <img
                className="d-block w-100"
                src="/front1.jpg"
                alt="First slide"
              />
              <Carousel.Caption>
                <h1>In Any Terrain</h1>
                <h4>Passionate and Dedicated</h4>
              </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className="carousels">
            <img
              className="d-block w-100"
              src="/front5.jpg"
              alt="Third slide"
            />

            <Carousel.Caption>
              <h1 id="biker">Comfortably Durable</h1>
              <h3>Light, Durable and Versatile</h3>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className="carousels">
            <img
              className="d-block w-100"
              src="/front2.jpg"
              alt="Second slide"
            />
            <Carousel.Caption>
              <h1>Even Under the Water</h1>
              <p>The Standard in Versatility and Durability</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className="carousels">
            <img
              className="d-block w-100"
              src="/front3.jpg"
              alt="slide"
            />
            <Carousel.Caption>
              <h1>Dedicated to the Mountains</h1>
              <p>Highly in demand</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
        </div>

       
        <div id="aboutLakbay" className="pt-4 pl-4 aboutUs">
          <h1>About Lakbay Sportswear</h1>
          <p className="aboutP mr-5 ml-5">
            Welcome to Lakbay Sportswear, your one stop shop for buying travel and sports 
            gears from outstanding brands. Our online shop offers a wide range of products
             for your comfortable online shopping experience. You will find everything from camping 
            tents, backpacks, trekking poles, swimming trunks, biking gears and many more 
            for the whole family with different sports preferences. Our goal is to let you enjoy your 
            travel or any sport endeavor while prioritizing safety in which you feel comfortable and enjoy 
            the outdoor activities. Customers' satisfaction is indeed in our absolute maximum.</p>
          </div>
        </div>
    )
  }
  
}

export default Home;