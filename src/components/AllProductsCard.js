import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function AllProductsCard({productsProp}) {


	return (		
					<Card className="cardTo">
						<Card.Body>
							<Card.Title>
								{productsProp.name}
							</Card.Title>
							 <Card.Img variant="top" className="prodImg" src={`/${productsProp._id}.JPG`} />
							<Card.Text>
								Price: {productsProp.price}
							</Card.Text>
							<Link to={`update/${productsProp._id}`} className="btn btn-secondary prodBtn">
								Update Product
							</Link>	
						</Card.Body>
					</Card>
					
		);
};