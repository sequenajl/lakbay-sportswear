import {Row, Col} from 'react-bootstrap';

export default function AdminBanner({data}){
	return(
		<Row>
			<Col>
				<h1 className="text-center mt-5">{data.title}</h1>
				<h4 className="text-center my-4">{data.content}</h4>
			</Col>
		</Row>

		);
}